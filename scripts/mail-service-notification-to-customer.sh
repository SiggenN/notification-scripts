#!/bin/sh

template=`cat <<TEMPLATE
You can not reply to this email.

Notification Type: $NOTIFICATIONTYPE

Service: $SERVICEDISPLAYNAME
Host: $HOSTALIAS
Address: $HOSTADDRESS
Status: $SERVICESTATE

Date/Time: $LONGDATETIME

Additional Info: $SERVICEOUTPUT

TEMPLATE
`

/usr/bin/printf "%b" "$template" | mail -s "$NOTIFICATIONTYPE: - $HOSTDISPLAYNAME - $SERVICEDISPLAYNAME is $SERVICESTATE" $USEREMAIL