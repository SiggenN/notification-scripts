#!/bin/sh

Icinga_URL=$(echo "https://icinga.netnordic.se/icingaweb2/monitoring/service/show?host=${HOSTNAME}&service=${SERVICEDESC}" | sed -e 's/ /%20/g')

template=`cat <<TEMPLATE

Notification Type: $NOTIFICATIONTYPE

Service: $SERVICEDISPLAYNAME
Host: $HOSTALIAS
Address: $HOSTADDRESS
Location: $HOSTLOCATION
Status: $SERVICESTATE

Date/Time: $LONGDATETIME

Additional Info: $SERVICEOUTPUT

Comment: [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT


URL: $Icinga_URL
TEMPLATE
`

/usr/bin/printf "%b" "$template" | mail -s "$NOTIFICATIONTYPE: - $HOSTDISPLAYNAME - $SERVICEDISPLAYNAME is $SERVICESTATE" $USEREMAIL
