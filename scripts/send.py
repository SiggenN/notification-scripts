import requests
import argparse
import sys

def sendSMS(auth, sender, to, message):
    send = requests.post(
        'https://api.46elks.com/a1/sms',
        auth = auth,
        data = {
            'from': sender,
            'to': to,
            'message': message,
        }
    )
    return send.text

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-m', '--message',
        type=str, help='Message you want to send to customer')
    parser.add_argument('-s', '--sender',
        type=str, default='NetNordic NOC', help='Text or specific number')
    parser.add_argument('-t', '--to',
        type=str, help='Number to send message to')
    parser.add_argument('-u', '--user',
        type=str, help='46elks API user')
    parser.add_argument('-p', '--password',
        type=str, help='46elks API token')

    args_stdin = parser.parse_args()

    auth = (args_stdin.user, args_stdin.password)

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
        args=parser.parse_args()
    else:
        sendSMS(auth, args_stdin.sender, args_stdin.to, args_stdin.message)

if __name__ == '__main__':
    main()
