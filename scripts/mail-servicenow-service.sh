#!/bin/sh

Icinga_URL=$(echo "https://icinga.netnordic.se/icingaweb2/monitoring/service/show?host=${HOSTNAME}&service=${SERVICEDESC}" | sed -e 's/ /%20/g')

template=`cat <<TEMPLATE

Notification Type: $NOTIFICATIONTYPE

Service: $SERVICEDISPLAYNAME
Host: $HOSTALIAS
Address: $HOSTADDRESS
Location: $HOSTLOCATION
Status: $SERVICESTATE

Date/Time: $LONGDATETIME

Additional Info: $SERVICEOUTPUT

Comment: [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT


URL: $Icinga_URL

<message_type>service_alert</message_type>
<service>$SERVICEDISPLAYNAME</service>
<host>$HOSTDISPLAYNAME</host>
<address>$HOSTADDRESS</address>
<alert_type>$NOTIFICATIONTYPE</alert_type>
<status>$SERVICESTATE</status>

TEMPLATE
`

/usr/bin/printf "%b" "$template" | mail -s "$NOTIFICATIONTYPE: - $HOSTDISPLAYNAME - $SERVICEDISPLAYNAME is $SERVICESTATE" $USEREMAIL
