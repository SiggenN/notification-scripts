#!/bin/sh

template=`cat <<TEMPLATE
You can not reply to this email.

Notification Type: $NOTIFICATIONTYPE

Host: $HOSTALIAS
Address: $HOSTADDRESS
Status: $HOSTSTATE

Date/Time: $LONGDATETIME

Additional Info: $HOSTOUTPUT

TEMPLATE
`

/usr/bin/printf "%b" "$template" | mail -s "$NOTIFICATIONTYPE: - $HOSTDISPLAYNAME is $HOSTSTATE" $USEREMAIL