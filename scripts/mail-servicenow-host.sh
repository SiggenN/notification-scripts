#!/bin/sh

Icinga_HOSTURL=$(echo "https://icinga.netnordic.se/icingaweb2/monitoring/host/show?host=${HOSTNAME}" | sed -e 's/ /%20/g')

template=`cat <<TEMPLATE

Notification Type: $NOTIFICATIONTYPE

Host: $HOSTALIAS
Address: $HOSTADDRESS
Location: $HOSTLOCATION
Status: $HOSTSTATE

Date/Time: $LONGDATETIME

Additional Info: $HOSTOUTPUT

Comment: [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT


URL: $Icinga_HOSTURL

<message_type>host_alert</message_type>
<host>$HOSTDISPLAYNAME</host>
<alert_type>$NOTIFICATIONTYPE</alert_type>
<address>$HOSTADDRESS</address>
<status>$HOSTSTATE</status>

TEMPLATE
`

/usr/bin/printf "%b" "$template" | mail -s "$NOTIFICATIONTYPE: - $HOSTDISPLAYNAME is $HOSTSTATE" $USEREMAIL

