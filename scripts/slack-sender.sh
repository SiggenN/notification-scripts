#!/bin/bash

ICINGA_HOSTNAME="icinga.netnordic.se"
SLACK_WEBHOOK_URL="https://hooks.slack.com/services/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"
SLACK_CHANNEL="#alerts"
SLACK_BOTNAME="Morgan Freeman"


if [ -z "$SERVICESTATE" ]
then
    STATE="${HOSTSTATE}"
else
    STATE="${SERVICESTATE}"
fi


#Set the message icon based on ICINGA service state
if [ "$STATE" = "CRITICAL" ]
then
    ICON=":bomb:"
elif [ "$STATE" = "WARNING" ]
then
    ICON=":warning:"
elif [ "$STATE" = "OK" ]
then
    ICON=":beer:"
elif [ "$STATE" = "UNKNOWN" ]
then
    ICON=":question:"
elif [ "$STATE" = "UP" ]
then
    ICON=":heavy_check_mark:"
elif [ "$STATE" = "DOWN" ]
then
    ICON=":x:"
else
    ICON=":white_medium_square:"
fi

#Message format
if [ -z "$SERVICEDISPLAYNAME" ]
then
    SERVICE_MESSAGE=""
else
    SERVICE_MESSAGE="*Service*: <https://${ICINGA_HOSTNAME}/icingaweb2/monitoring/service/show?host=${HOSTNAME}&service=${SERVICEDESC}|${SERVICEDISPLAYNAME}>\n"
fi

PAYLOAD="payload={\"channel\": \"${SLACK_CHANNEL}\", \"username\": \"${SLACK_BOTNAME}\", \"text\": \"*Host*: <https://${ICINGA_HOSTNAME}/icingaweb2/monitoring/host/services?host=${HOSTNAME}|${HOSTDISPLAYNAME}>\n${SERVICE_MESSAGE}*State*: ${STATE} ${ICON}\"}"

#Send message
curl --connect-timeout 30 --max-time 60 -s -S -X POST --data-urlencode "${PAYLOAD}" "${SLACK_WEBHOOK_URL}"
