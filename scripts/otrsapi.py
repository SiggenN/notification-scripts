#!/usr/bin/env python3
import logging.config
import requests
import re
import sys
import os
try: # for Python 3
    from http.client import HTTPConnection
except ImportError:
    from httplib import HTTPConnection
from optparse import OptionParser
from urllib.parse import quote


class OtrsClient:
    def __init__(self, ip, service,params,debug=None):
        self.ip = ip
        self.params = params
        self.baseurl='https://%(ip)s/otrs/nph-genericinterface.pl/Webservice/%(service)s' % {'ip':ip,'service':service}
        self.debug=debug
    def ticket_search(self,search_dict={'StateType':'open'}):
        url=self.baseurl+'/TicketSearch'
        ticketdata=dict(self.params, **search_dict)
        res=requests.post(url,json=ticketdata,verify=False)
        if self.debug:
            print('\nSearch:'+str(res.json())+'\n')
        return res
    def new_ticket(self,title,article_stuff):
        url=self.baseurl+'/TicketCreate'
        ticket_defaults= {"Ticket":{"Queue":"Monitor","Title":title,"Priority":"P3 Normal","State":"new","CustomerUser":"apiicinga","Type":"Incident"}}
        ticket_defaults.update(self.params)
        ticketdata=dict(ticket_defaults,**article_stuff)
        res=requests.post(url,json=ticketdata,verify=False)
        if res and self.debug:
            print('\nNew:'+str(res.json())+'\n')
        return res
    def ticket_update(self,tickid,article_stuff,ticket_stuff):
        url=self.baseurl+'/TicketUpdate'
        ticketdata=dict(self.params, **article_stuff)
        ticketdata.update({'TicketID':tickid})
        #ticketdata.update(ticket_stuff)
        if ticket_stuff:
            ticketdata.update(ticket_stuff)
        res=requests.post(url,json=ticketdata,verify=False)
        if res and self.debug:
            print('\nUpdate'+str(res.json())+'\n')
        return res
    def ticket_get(self,tickid):
        url=self.baseurl+'/TicketGet'
        ticketdata={'TicketID':tickid}
        ticketdata.update(self.params)
        res=requests.post(url,json=ticketdata,verify=False)
        if res and self.debug:
            print('\nGet:'+str(res.json())+'\n')
        return res



def debug_on():
    HTTPConnection.debuglevel = 1
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

def debug_off():
    HTTPConnection.debuglevel = 0
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.WARNING)
    root_logger.handlers = []
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.WARNING)
    requests_log.propagate = False


def main():
    parser = OptionParser()
    parser.add_option("-s", "--server",
                  action="store", dest="ip", default='ticket.netnordic.se',
                  help="otrs IP")
    parser.add_option("-u", "--user",
                  action="store", dest="user", default='ldap.se',
                  help="otrs user")
    parser.add_option("-p", "--pass",
                  action="store", dest="password", default='U4j9367HIy7B',
                  help="otrs pass")
    parser.add_option("-d", "--debug",
                  action="store", dest="deb", default=None,
                  help="set to true for debugging, default off")
    parser.add_option("-H", "--host",
                  action="store", dest="host",
                  help="host name for fields in otrs")
    parser.add_option("-S", "--service",
                  action="store", dest="service", default=None,
                  help="Monitoring service for fields in otrs")
    parser.add_option("-t", "--type",
                  action="store", dest="type", default=None,
                  help="type of notification (problem/recovery)")
    parser.add_option("-e", "--state",
                  action="store", dest="state", default=None,
                  help="the state of the service/host")
    parser.add_option("-a", "--hostaddress",
                  action="store", dest="address", default=None,
                  help="address of the host")
    parser.add_option("-D", "--date",
                  action="store", dest="time", default=None,
                  help="datetime from icinga")
    parser.add_option("-o", "--output",
                  action="store", dest="output", default=None,
                  help="service/host plugin output")
    (options, args) = parser.parse_args()
    if options.deb:
        debug_on()
    if options.service:
        search_string = 'PROBLEM: - %(host)s - %(service)s' % {'service':options.service,'host':options.host}
        subject = '%(type)s: - %(host)s - %(service)s is %(state)s' % {'service':options.service,'host':options.host,'state':options.state,'type':options.type}
        #url https://icinga.netnordic.se/monitoring/service/show?host=${HOSTALIAS}&service=${SERVICEDESC}
        body = '''
Notification Type: %(type)s

Service: %(service)s
Host: %(host)s
Address: %(address)s
Status: %(state)s

Date/Time: %(date)s

Additional Info: %(output)s

Comment: []


URL: %(url)s''' % {'service':options.service,'host':options.host,'state':options.state,'type':options.type, 'address':options.address, 'date':options.time, 'output':options.output, 'url': 'https://icinga.netnordic.se/icingaweb2/monitoring/service/show?host='+quote(os.environ.get('HOSTNAME',options.host))+'&service='+quote(os.environ.get('SERVICEDESC',options.service))}
    else:
        search_string = 'PROBLEM: - %(host)s is DOWN' % {'host':options.host}
        subject = '%(type)s: - %(host)s is %(state)s' % {'type':options.type,'service':options.service,'host':options.host,'state':options.state}
        body = '''
Notification Type: %(type)s

Host: %(host)s
Address: %(address)s
Status: %(state)s

Date/Time: %(date)s

Additional Info: %(output)s

Comment: []


URL: %(url)s''' % {'host':options.host,'state':options.state,'type':options.type, 'address':options.address, 'date':options.time, 'output':options.output, 'url': 'https://icinga.netnordic.se/icingaweb2/monitoring/host/show?host='+quote(os.environ.get('HOSTNAME',options.host))}
    search_dict = {'Result':'HASH','ContentSearch':'AND', 'ContentSearchPrefix':'','ContentSearchSuffix':'*','StateType':['new', 'open', 'pending reminder', 'pending auto'], 'From':'icinga-apirequest@netnordic.se', 'Subject':search_string, 'Queues':['NNSE NOC', 'Accessbolaget', 'Monitor', 'TAC'], 'UseSubQueues': 1}
    #'States':['new', 'open', 'pending reminder', 'pending auto close+', 'pending auto close-', 'Pending Response From Customer']


    icinga = OtrsClient(ip=options.ip,service='icinga',params={'UserLogin':options.user,'Password':options.password},debug=options.deb)
    conn=icinga.ticket_search(search_dict)
    if not (conn.status_code == 200):
        sys.exit(1)
    resp = conn.json()
    article_dict={"Article":{"TimeUnit": 0, "SenderType":"agent","ArticleType":"email-internal","From":"icinga-apirequest@netnordic.se","Subject":subject,"Body":body,"ContentType":"text/plain; charset=utf8"}}
    if not resp and not options.type == 'RECOVERY': #otrs returns an empty result if no ticket is found instead of empty ticketid array. dumb but nothing to do.
        icinga.new_ticket(subject,article_dict)
    elif 'TicketID' in resp:
        if len(resp['TicketID']) == 1:
            #print('\nTicketID = '+resp['TicketID'][0]+'\n')
            ticket_dict={'Ticket':{'State':'new'}}
            tick_details=icinga.ticket_get(resp['TicketID'][0])
            if tick_details.status_code == 200:
                try:
                    tick=tick_details.json()['Ticket'][0]
                    if options.service and re.search("Temperature|Traffic",options.service):
                        if re.search("[pP]ending", tick['State']):
                            ticket_dict=None
                    elif re.search("[pP]ending|[oO]pen", tick['State']):
                        ticket_dict={'Ticket':{'State':'open'}}
                except KeyError:
                    pass
            icinga.ticket_update(resp['TicketID'][0],article_dict,ticket_dict)
        else:
            print ('found more than one open ticket, exiting...')
            sys.exit(1)

if __name__ == "__main__":
    main()
